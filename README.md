# README #

This is a T9 decoder for the [osix.net](http://www.osix.net) bonus challenge 'Keytone Snooping'

### Challenge Description ###

Now pay attention. In a cliche 50's HG Wells sci-fi style, aliens have begun to infiltrate our society with the aim of farming us for our knee caps, a delicacy in some parts of the universe. Keen to fit in with the youth of today, the aliens have chosen the ever popular SMS text message as their prefered form of communication with their shiny mothers on their shiny mother ship. Fortunately for us humans the alien beings haven't quite got to grips with walking and texting at the same time, although to be fair this is a genetic default due to there thumbs and legs sharing the same nervous synapsis. It was precisely because of this unfortunate condition that unemployed pirate James Grrrr found himself stuck behind a stationary alien in the entrance to a lift.

J.Grrr never left home without his parrot.

Whilst Grrr was giving himself a hernia trying to move the motionless alien out of his way his parrot, with it's love of electronica, was listening avidly to the beats being produced by the alien's handset.

Loving the music so and keen to be able to recite it to his parrot DJ friends the parrot memorized the lot and spent the rest of the day repeating the tune out loud to weary passers-by.

Unbeknownst to all a surveillance camera had been at work in the lift (installed by the city police to try and solve the case of the silent but deadly murderers). Seeing the footage and wishing to find out what was happening, the police called at Grrr's door and confiscated his parrot. After signing the parrot to a four year deal the police eventually managed to extract the tones and deduce a key press sequence, which can be found here.

It is known that all aliens like to use suggestive texting, 'T9' to use it's trade name). Your mission is to decode these key presses back into english (yes they speak english.) Here is a brief description of how T9 works on the outworlder's handset.

The letters on the phone's keypad are laid out thus:

```
#!

1 => .
2 => abc
3 => def,
4 => ghi,
5 => jkl,
6 => mno,
7 => pqrs,
8 => tuv,
9 => wxyz,
0 => space
```

The phone contains a dictionary, for the purpose of examples we'll use the following:

```
#!

add ade ape apple ban banana bandana bap bar born cap car carrot cart do dog donut he hoop id ie inns mono moon nod noon one page paid pint pump puns queue rage raid riot rump runs said sage shot steve stop sums suns tan tao tom van
```

The user presses the key for each letter just once. To spell 'apple' the user would press 27753. However if the keypresses fit more than one word then the user must press '*' to cycle though the possible words. These are presented in alphabetical order.

```
#!

Examples:
"apple" => 27753
"add ade" => 2330233*0
"apple bar car" => 277530227*0227***
"riot. rump banana pump raid." => 7468*107867**02262620786707243***10
```

In real life the phone has a much bigger dictionary which you can find here.
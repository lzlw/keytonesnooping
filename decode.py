#!/usr/bin/python

charset = {
        "1": ".",
        "2": "abc",
        "3": "def",
        "4": "ghi",
        "5": "jkl",
        "6": "mno",
        "7": "pqrs",
        "8": "tuv",
        "9": "wxyz",
        "0": " "
        }

encodedDictionary = { }
codedSet = ''
decodedString = ''

def decodeChunk(codedWord, depth, codedSet):
    if codedWord == '':
        return ''

    if type(codedSet[codedWord]) == list:
        return codedSet[codedWord][depth]
    else:
        return codedSet[codedWord]

with open('./tones.txt') as codedFile:
    codedSet = codedFile.read()

# read dictionary into list
with open('./words') as dictFile:
    rawDictionary = [x.strip('\n') for x in dictFile.readlines()]

for rawWord in rawDictionary:
    encodedWord = ''
    for char in rawWord.lower():
        for key, value in charset.iteritems():
            if char in value:
                encodedWord += key

    if encodedDictionary.has_key(encodedWord):
        if type(encodedDictionary[encodedWord]) != list:
            encodedDictionary[encodedWord] = [encodedDictionary[encodedWord], rawWord]
        else:
            encodedDictionary[encodedWord].append(rawWord)
    else:
        encodedDictionary[encodedWord] = rawWord

'''
Loop through each character of the encoded string and add it our encoded word.

If we hit a *, we know that we have to increase our search depth.

When we reach a 1 or a 0, we know the word is over, and can send it to decodeChunk()
'''

depth = 0
codedWord = ''

for i in range(len(codedSet)):
    if codedSet[i] == '1':
        decodedString += decodeChunk(codedWord, depth, encodedDictionary)
        codedWord = ''
        depth = 0
        decodedString += '.'
    elif codedSet[i] == '0':
        decodedString += decodeChunk(codedWord, depth, encodedDictionary)
        codedWord = ''
        decodedString += ' '
        depth = 0
    elif codedSet[i] == '*':
        depth += 1
    else:
        codedWord += codedSet[i]

print(decodedString)
